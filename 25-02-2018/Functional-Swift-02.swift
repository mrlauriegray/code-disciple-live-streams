// iOS Dating App //
// Date-iOS App //
// Users can send up their rating for themselves
// Community responds with actual rating
// Micro-transaction bonus feature: allows you to change the rating that the community did -> if you've paid
// Check mutableContainers

import Foundation
// I'll rate myself a 5?
// Community will return a 2/10
typealias UserAttributeResult = (Int) -> Int

let apiURL = "www.date-ios-app.com/api/v2.5/userrating-348932-8d9fd-dsa98s-a/"

func connectToWeb(withURL url: String, withDesiredUserRating rating: Int) -> Int {
  let formattedURL = URL(string: apiURL)!
  var result: Int?
  let dataTask = URLSession.shared.dataTask(with: formattedURL) { (data, response, error) in
    do {
      guard let someData = data else { return }
      guard let json = try JSONSerialization.jsonObject(with: someData, options: .mutableContainers) as? [String: Any] else { return }
      // Post the data, users would get a notification, the top 5 ratings would come back down to users as an average
      // "averageRating": 3
      result = 3
    } catch let error {
      print(error)
    }
  }
  //  dataTask.resume()
  // If the community responded
  if let result = result {
    return result
  }
  // Else return the normal rating
  return 3
}

// Closure to use
// Using the function above
// Using the URL Above it
let goOnlineAndUpdateResult: UserAttributeResult = { (rating: Int) in
  let ratedAttribute = connectToWeb(withURL: apiURL, withDesiredUserRating: rating)
  return ratedAttribute
}

let alertUser = { print("New Result has just came in") }

// Swift by Sundell //
func requestUserResult(_ closure: @escaping UserAttributeResult, then completion: @escaping () -> ()) -> UserAttributeResult {
  
  return { submittedValue in
    // Go online and fetch the result
    // When you're done do this:
    let calculatedResult = closure(submittedValue)
    completion()
    return calculatedResult
  }
}
// User taps 'submit' button with value of 5 for self-rating
let finalUserResult = requestUserResult(goOnlineAndUpdateResult, then: alertUser)(5)
